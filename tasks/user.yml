---

# create Linux user account for Gitea
# and add the Gitea username to AllowUsers directive in sshd_config

# by omitting the password parameter in this task, we effectively create the account with "--disable-password"
# https://stackoverflow.com/a/39018859/1198249
- name: "Create {{ gitea.user.name }} system user and group"
  ansible.builtin.user:
    name: "{{ gitea.user.name }}"
    home: "/home/{{ gitea.user.name }}"
    system: yes
    shell: /bin/bash
    comment: "Gitea system user"
    state: present

# The gitea user MUST be added to AllowUsers in /etc/ssh/sshd_config
# otherwise all git operations will simply fail with
# > gitea@<hostname>: Permission denied (publickey).
# > fatal: Could not read from remote repository.
- name: "Reset AllowUsers in /etc/ssh/sshd_config (add {{ gitea.user.name }})"
  ansible.builtin.lineinfile:
    dest: /etc/ssh/sshd_config
    regexp: "^AllowUsers"
    line: "AllowUsers {{ ssh_allow_users |  join(' ') }}"
    state: present
  notify: Restart SSH

# I chose this approach because I could not figure out how to achieve the same thing with regex
# In the end, I think this approach is easier to understand than a regex with capture groups and stuff
# - name: "Make sure SSH daemon AllowUsers includes {{ gitea.user.name }}"
#   ansible.builtin.replace:
#     path: /etc/ssh/sshd_config
#     # capture everything from AllowUsers to end of line (if "gitea" is not found),
#     # then replace \1 and append "gitea"
#     # if "gitea" is found, nothing more to do
#     # yeah, I cant figure out this regex
#     # (?<!) matches the preceding string (look behind)
#     # .*(?<!gitea)
#     regexp: '(AllowUsers (?:(?!gitea).)*)(?:gitea)'
#     replace:
