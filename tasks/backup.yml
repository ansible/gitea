---

# Backup the Gitea databases to BAY via the Ansible controller

- name: Timestamp this backup
  ansible.builtin.set_fact:
    # iso8601_basic_short should look like 20220122T223743
    this_backup_timestamp: "{{ ansible_date_time.iso8601_basic_short }}"

- name: Set backup folder name
  ansible.builtin.set_fact:
    this_backup_name: "{{ role_name }}-{{ this_backup_timestamp }}"

- name: Set temporary backup directory
  ansible.builtin.set_fact:
    this_backup_tmp: "{{ ansible_env.HOME }}/.cache/{{ this_backup_name }}"


- name: Backup Gitea databases
  block:

    # this is not pretty, and hard-codes the number of instances, but I could not make it work otherwise
    - name: "Temporary SQL dump paths on target"
      ansible.builtin.set_fact:
        this_sqlgz_paths:
          - "{{ ansible_env.HOME }}/backups/{{ gitea.instances | first }}-{{ this_backup_timestamp }}.sql.gz"
          - "{{ ansible_env.HOME }}/backups/{{ gitea.instances | last }}-{{ this_backup_timestamp }}.sql.gz"

    - name: "Set where database dump will be temporarily saved"
      ansible.builtin.set_fact:
        this_backup_database_dump:
          database: "{{ gitea_db_names | list }}"
          bkp_path: "{{ this_sqlgz_paths | list }}"

    # The rest of these tasks manage to not hard-code the number of databases...
    # I don't see that the Ansible module mysql_db can replace this command
    # note: dumps db and compresses on-the-fly before saving to disk
    # When using the default InnoDB engine, it is important to use the `--single-transaction` flag
    # https://mariadb.com/kb/en/mariadb-backups-overview-for-sql-server-users
    # https://mysqldump.guru/run-mysqldump-without-locking-the-tables.html
    # https://stackoverflow.com/a/41704768
    # https://serverfault.com/q/231300
    # In light of the above links, and the fact that we are using InnoDB engine, I decided to
    # add --single-transaction and remove --lock-tables (and explicitly add --skip-lock-tables)
    # NOTE: this dump command must not be used with MyISAM databases, it expects InnoDB engine!
    - name: Backup (mysqldump) Gitea databases
      ansible.builtin.shell: >
        mysqldump
        --single-transaction
        --skip-lock-tables
        --default-character-set=utf8mb4
        -u {{ gitea.database.user }}
        -p{{ gitea.database.password }}
        {{ this_backup_database_dump.database[idx] }} | gzip >
        {{ this_backup_database_dump.bkp_path[idx] }}
      no_log: true
      # was not possible to loop over this_backup_database_dump, because it is not a list at top-level
      loop: "{{ gitea.instances }}"
      loop_control:
        index_var: idx
      become: true
      become_user: root

    # https://docs.ansible.com/ansible/latest/collections/ansible/builtin/fetch_module.html
    # unless the database is compressed, this fetch takes forever (even with compression it takes a while)
    - name: Copy the compressed database to backup directory on BAY
      ansible.builtin.fetch:
        src: "{{ this_backup_database_dump.bkp_path[idx] }}"
        # I tried using | or > or |> to allow splitting this long line, but the
        # resulting filename always ends up containing weird characters
        dest: "{{ backup_root_path }}/hosts/{{ inventory_hostname }}/{{ this_backup_name }}/{{ this_backup_database_dump.database[idx] }}.sql.gz"
        flat: yes
      loop: "{{ gitea.instances }}"
      loop_control:
        index_var: idx
      # if fetch is run with become the transfer size is effectively doubled
      # https://docs.ansible.com/ansible/latest/collections/ansible/builtin/fetch_module.html#notes
      become: false

    - name: "Remove the temporary dumps from remote"
      ansible.builtin.file:
        path: "{{ this_backup_database_dump.bkp_path[idx] }}"
        state: absent
      loop: "{{ gitea.instances }}"
      loop_control:
        index_var: idx

    - name: Encrypt the database dumps
      ansible.builtin.command: >
        gpg --encrypt --recipient '{{ gpg_key }}'
        {{ backup_root_path }}/hosts/{{ inventory_hostname }}/{{ this_backup_name }}/{{ this_backup_database_dump.database[idx] }}.sql.gz
      loop: "{{ gitea.instances }}"
      loop_control:
        index_var: idx
      become: true
      become_user: "{{ ansible_env.USER }}"
      delegate_to: localhost

    - name: Delete the plain-text database dumps
      ansible.builtin.file:
        path: "{{ backup_root_path }}/hosts/{{ inventory_hostname }}/{{ this_backup_name }}/{{ this_backup_database_dump.database[idx] }}.sql.gz"
        state: absent
      loop: "{{ gitea.instances }}"
      loop_control:
        index_var: idx
      become: true
      become_user: "{{ ansible_env.USER }}"
      delegate_to: localhost
  # END OF BLOCK


- name: Backup Gitea files
  block:

    - ansible.builtin.set_fact:
        gitea_archive_name: gitea_files.tar

    - ansible.builtin.set_fact:
        gitea_archive_tmp: "{{ ansible_env.HOME }}/.cache/{{ gitea_archive_name }}"
        # bz2 -> 2.4 GB
        # gz  -> 2.4 GB
        # tar -> "not valid format" error, task fails
        # xz  -> "module failure" error, task fails
        # zip -> 2.4 GB
        # considering that the uncompressed size is ca 2.6 GB, I'm not at all
        # impressed by these results
        gitea_archive_format: gz

    # concatenate list of gitea.path into a space-separated string, suitable
    # for use as arguments with tar
    # Ok, works, only cosmetic issue: trailing space after the last item
    # - ansible.builtin.set_fact:
    #     gitea_paths: "{{ item.value ~ ' ' ~ gitea_paths | default('') }}"
    #   loop: "{{ gitea.path | dict2items }}"

    # create a list variable with all the paths (values, not keys) from gitea.path
    # Ok, works great
    # Now we can provide this list to the community.general.archive.path and create
    # our archive in one go.
    - ansible.builtin.set_fact:
        gitea_paths: "{{ gitea_paths | default([]) + [ item.value ] }}"
      loop: "{{ gitea.path | dict2items }}"

    # complication here is that gitea.path is not a list, but a dictionary (named list)
    # the trick is to extract just the values, without the keys, as a list variable
    # `gitea.path | list` produces a list, but extracts the keys, not the values
    # We can use `gitea.path | dict2items` to extract only values, but not without looping
    # https://docs.ansible.com/ansible/latest/collections/community/general/archive_module.html
    - name: "Backup Gitea's directories (compressed with {{ gitea_archive_format }})"
      # note! path and dest on the remote host!
      community.general.archive:
        path: "{{ gitea_paths }}"
        dest: "{{ gitea_archive_tmp }}"
        format: "{{ gitea_archive_format }}"
        owner: "{{ ansible_env.USER }}"
        group: "{{ ansible_env.USER }}"
        mode: ug=rw,o=r
      # looping this task is counter-productive, as each iteration overwrites the existing archive (does not append)
      # we end up with a tarball of just the last item! this is no good.
      # no issues on this overwriting behaviour subject, as far as I can tell
      # https://github.com/ansible-collections/community.general/issues
      # loop: "{{ gitea.path | dict2items }}"

    # https://docs.ansible.com/ansible/latest/collections/ansible/builtin/fetch_module.html
    # NOTE! This compressed archive is expected to be a few GBs large,
    # which makes it larger than the total RAM on the remote
    # https://github.com/ansible/ansible/issues/11702
    # "it is advisable to run fetch tasks with become: false whenever possible"
    # note that even with swap configured, it appears not to be used by fetch/slurp (only RAM matters)
    # https://stackoverflow.com/questions/31580319/large-file-of-ansible-fetch-module
    - name: Copy the compressed archive to backup directory on BAY
      ansible.builtin.fetch:
        src: "{{ gitea_archive_tmp }}"
        dest: "{{ backup_root_path }}/hosts/{{ inventory_hostname }}/{{ this_backup_name }}/{{ gitea_archive_name }}"
        flat: yes
      # this remote file is owned by our username, so we can use `become: false`
      # note: this appears to have resolved the bug, even when tarball is larger than total RAM
      # if fetch is run with "become" the transfer size is effectively doubled
      # https://docs.ansible.com/ansible/latest/collections/ansible/builtin/fetch_module.html#notes
      become: false
      # fetch output:
        # "changed": true,
        # "checksum": "08e6025303adc050d8e74ccbd6abe6debc05feb0",
        # "dest": "{{ backup_root_path }}/hosts/{{ inventory_hostname }}/{{ this_backup_name }}/{{ gitea_archive_name }}",
        # "failed": false,
        # "md5sum": "a6e3677a1d2e8b57fb133b5a93dcf921",
        # "remote_checksum": "08e6025303adc050d8e74ccbd6abe6debc05feb0",
        # "remote_md5sum": null
      register: gitea_files_copy

    - name: Remove the compressed archive from the remote
      ansible.builtin.file:
        path: "{{ gitea_archive_tmp }}"
        state: absent
      when: not (gitea_files_copy.failed | bool)

    - name: Encrypt the compressed archive
      ansible.builtin.command: >
        gpg --encrypt --recipient '{{ gpg_key }}'
        {{ backup_root_path }}/hosts/{{ inventory_hostname }}/{{ this_backup_name }}/{{ gitea_archive_name }}
      become: true
      become_user: "{{ ansible_env.USER }}"
      delegate_to: localhost

    - name: Delete the plain-text compressed archive
      ansible.builtin.file:
        path: "{{ backup_root_path }}/hosts/{{ inventory_hostname }}/{{ this_backup_name }}/{{ gitea_archive_name }}"
        state: absent
      become: true
      become_user: "{{ ansible_env.USER }}"
      delegate_to: localhost
  # END OF BLOCK


# record metadata about the installed Gitea installation into a text file in the backup directory
# currently only saving Gitea version, but this approach should be easier to extend with other
# metadata than prior method which added metadata to filename itself
- name: Backup metadata
  block:

    # $ gpg --list-keys --fingerprint
    # /home/taha/.gnupg/pubring.gpg
    # -----------------------------
    # pub   rsa2048 2022-11-04 [SC]
    #       9505 6773 3548 5B28 A847  6BDF C325 4385 3412 945B
    # uid           [ultimate] Your Name <name@domain.se>
    # sub   rsa2048 2022-11-04 [E]
    # we could use tr "\n" " " instead of awk, but tr does not allow the replacement
    # to be empty string, which awk does
    - name: Store fingerprint of GPG pubkey
      ansible.builtin.shell: >
        gpg --list-keys --fingerprint | grep -A1 "^pub" | awk '{print}' ORS=''
      delegate_to: localhost
      become: true
      become_user: "{{ ansible_env.USER }}"
      register: gpg_key_verbose

    # note: for some reason "\\s{3,6}" had no effect
    - ansible.builtin.set_fact:
        gpg_key_fingerprint: '{{ gpg_key_verbose.stdout | regex_replace(" {3,6}", " ") }}'

    # get the version of the installed Gitea binary
    # because gitea_version is not necessarily the same version as installed binary
    - name: Get version of installed Gitea binary
      ansible.builtin.shell: gitea --version | awk '{print $3}'
      register: giversion

    - name: Create backup metadata file
      ansible.builtin.copy:
        owner: "{{ ansible_env.USER }}"
        group: "{{ ansible_env.USER }}"
        mode: ug=rw,o=r
        dest: "{{ backup_root_path }}/hosts/{{ inventory_hostname }}/{{ this_backup_name }}/metadata.backup"
        content: |
          gitea_version: {{ giversion.stdout }}
          encryption_key_uid: {{ gpg_key }}
          encryption_key_fingerprint: {{ gpg_key_fingerprint }}
      delegate_to: localhost
      become: true
      become_user: "{{ ansible_env.USER }}"
  # END OF BLOCK
